const { server } = require('./App/App');
const { server_port } = require('./config/config');

server.listen(server_port, () => {
  console.log(`Server is live on http://localhost:${server_port}`);
});
