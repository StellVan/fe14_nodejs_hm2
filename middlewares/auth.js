const jwt = require('jsonwebtoken');
const { secret } = require('../config/config');

module.exports.auth = (req, res, next) => {
  const authHeader = req.headers['auth'];

  if (!authHeader) {
    return res.status(401).json({ status: 'No such auth!' });
  }

  const [, jwtoken] = authHeader.split(' ');

  try {
    req.user = jwt.verify(jwtoken, secret);
    next();
  } catch (err) {
    return res.status(401).json({ status: 'Invalid JWT' });
  }
};
