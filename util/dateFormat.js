module.exports.formatDate = (value) => {
  return value > 9 ? value : '0' + value;
};
