const { noteModel } = require('../models/models');
const { formatDate } = require('../util/dateFormat');

module.exports.addNote = (req, res) => {
  const { text } = req.body;
  const userId = req.user._id;

  if (!text) {
    return res.status(400).json({ message: 'No text was provided' });
  }

  const date = new Date();
  const dateString = `${formatDate(date.getHours())}:${formatDate(
    date.getMinutes()
  )} ${formatDate(date.getDate())}/${formatDate(
    date.getMonth()
  )}/${date.getFullYear()}`;

  const note = new noteModel({
    userId: userId,
    completed: false,
    text: text,
    createdDate: dateString,
  });

  note
    .save()
    .then(() => {
      res.json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.getAllNotes = (req, res) => {
  const userId = req.user._id;
  const notesList = noteModel.find({ userId: userId });

  notesList
    .then((data) => {
      res.json({ notes: data });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.getNoteById = (req, res) => {
  const userId = req.user._id;
  const noteId = req.params.id;

  const notesList = noteModel.find({
    _id: noteId,
    userId: userId,
  });

  notesList
    .then((data) => {
      if (data.length === 0) {
        return res.status(400).json({ message: 'No such note' });
      }
      res.json({ note: data });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.deleteNote = (req, res) => {
  const userId = req.user._id;
  const noteId = req.params.id;

  const notesList = noteModel.deleteOne({
    _id: noteId,
    userId: userId,
  });

  notesList
    .then((data) => {
      if (data.deletedCount === 0) {
        return res.status(400).json({ message: 'No such note' });
      }
      res.json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.updateNote = (req, res) => {
  const { text } = req.body;
  const userId = req.user._id;
  const noteId = req.params.id;

  if (!text) {
    return res.status(400).json({ message: 'No text was provided' });
  }

  const notesList = noteModel.updateOne(
    {
      _id: noteId,
      userId: userId,
    },
    { text: text }
  );

  notesList
    .then((data) => {
      if (data.nModified === 0) {
        return res.status(400).json({ message: 'No such note' });
      }
      res.json({ message: 'Success' });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.switchNote = (req, res) => {
  const userId = req.user._id;
  const noteId = req.params.id;

  const note = noteModel.find({
    _id: noteId,
    userId: userId,
  });

  note
    .then((data) => {
      if (data.length === 0) {
        return res.status(400).json({ message: 'No such note' });
      }
      let check = !data[0].completed;

      const updatedNote = noteModel.updateOne(
        {
          _id: noteId,
          userId: userId,
        },
        {
          completed: check,
        }
      );

      updatedNote
        .then(() => {
          res.json({ message: 'Success' });
        })
        .catch((err) => {
          res.status(500).json({ message: err.message });
        });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};
