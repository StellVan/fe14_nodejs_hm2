const { userModel, noteModel, credModel } = require('../models/models');

module.exports.showUserProfile = (req, res) => {
  const userId = req.user._id;

  userModel
    .find({ _id: userId })
    .exec()
    .then((data) => {
      console.log(data);
      const { _id, username, createdDate } = data[0];
      res.json({
        user: {
          _id: _id,
          username: username,
          createdDate: createdDate,
        },
      });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.deleteUserProfile = (req, res) => {
  const userId = req.user._id;
  const username = req.user.username;
  userModel
    .deleteOne({ _id: userId })
    .exec()
    .then((data) => {
      if (data.deletedCount === 0) {
        return res.status(400).json({ message: 'No such user' });
      }
      noteModel
        .deleteMany({ userId: userId })
        .exec()
        .then(() => {
          credModel
            .deleteOne({ username: username })
            .exec()
            .then(() => {
              res.json({ message: 'Success' });
            })
            .catch((err) => {
              res.status(500).json({ message: err.message });
            });
        })
        .catch((err) => {
          res.status(500).json({ message: err.message });
        });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.changeUserPassword = (req, res) => {};
