const { secret } = require('../config/config');
const { userModel, credModel } = require('../models/models');
const { formatDate } = require('../util/dateFormat');
const jwt = require('jsonwebtoken');

module.exports.register = (req, res) => {
  const { username, password } = req.body;

  if (!username && !password) {
    return res
      .status(400)
      .json({ message: 'No username and no password were provided' });
  }
  if (!username) {
    return res.status(400).json({ message: 'No username was provided' });
  }
  if (!password) {
    return res.status(400).json({ message: 'No password was provided' });
  }

  const date = new Date();
  const dateString = `${formatDate(date.getHours())}:${formatDate(
    date.getMinutes()
  )} ${formatDate(date.getDate())}/${formatDate(
    date.getMonth()
  )}/${date.getFullYear()}`;

  const cred = new credModel({
    username: username,
    password: password,
  });

  cred
    .save()
    .then(() => {
      const user = new userModel({
        username: username,
        createdDate: dateString,
      });

      user
        .save()
        .then(() => {
          res.json({ message: 'Success' });
        })
        .catch((err) => {
          res.status(500).json({ message: err.message });
        });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.login = (req, res) => {
  const { username, password } = req.body;

  if (!username && !password) {
    return res
      .status(400)
      .json({ message: 'No username and no password were provided' });
  }
  if (!username) {
    return res.status(400).json({ message: 'No username was provided' });
  }
  if (!password) {
    return res.status(400).json({ message: 'No password was provided' });
  }

  credModel
    .findOne({ username, password })
    .exec()
    .then((user) => {
      if (!user) {
        return res.json({ message: 'No such user!' });
      }
      userModel
        .findOne({ username })
        .exec()
        .then((data) => {
          res.json({ jwt_token: jwt.sign(JSON.stringify(data), secret) });
        });
    })
    .catch((err) => {
      res.status(500).json({ status: err.message });
    });
};
