const express = require('express');
const router = express.Router();
const {
  addNote,
  getAllNotes,
  getNoteById,
  deleteNote,
  updateNote,
  switchNote,
} = require('../controllers/noteController');
const { auth } = require('../middlewares/auth');

router.post('/', auth, addNote);
router.get('/', auth, getAllNotes);
router.get('/:id', auth, getNoteById);
router.delete('/:id', auth, deleteNote);
router.put('/:id', auth, updateNote);
router.patch('/:id', auth, switchNote);

module.exports = {
  noteRouter: router,
};
