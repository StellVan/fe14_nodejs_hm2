const express = require('express');
const router = express.Router();
const {
  showUserProfile,
  changeUserPassword,
  deleteUserProfile,
} = require('../controllers/userController');
const { auth } = require('../middlewares/auth');

router.get('/me', auth, showUserProfile);
router.post('/me', auth, changeUserPassword);
router.delete('/me', auth, deleteUserProfile);

module.exports = {
  userRouter: router,
};
