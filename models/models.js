const mongoose = require('mongoose');

module.exports = {
  noteModel: mongoose.model('note', {
    userId: {
      required: true,
      type: String,
    },
    completed: {
      required: true,
      type: Boolean,
    },
    text: {
      required: true,
      type: String,
    },
    createdDate: {
      required: true,
      type: String,
    },
  }),

  userModel: mongoose.model('user', {
    username: {
      required: true,
      type: String,
      unique: true,
    },
    createdDate: {
      required: true,
      type: String,
    },
  }),

  credModel: mongoose.model('credential', {
    username: {
      required: true,
      type: String,
      unique: true,
    },
    password: {
      required: true,
      type: String,
    },
  }),
};
