const express = require('express');
const app = express();
const mongoose = require('mongoose');
const { noteRouter } = require('../router/noteRouter');
const { authRouter } = require('../router/authRouter');
const { userRouter } = require('../router/userRouter');
const { databaseURL } = require('../config/config');

mongoose.connect(databaseURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

app.use(express.json());

app.use('/api/notes', noteRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);

module.exports = {
  server: app,
};
